for x in range(5, 13, 2):
    print(x)

print(range(5))
print(list(range(5)))

my_string = 'скукотища'
index = 0
for value in my_string:
    print(index, value)
    index += 1

for item in enumerate(my_string):
    print(item)
for index, value in enumerate(my_string):
    print(index, value)

print(min(1, 3, 56, 4))
print(max(1, 3, 56, 4))

my_list = [1, 3, 56, 4]
print(min(my_list))

print(len(my_list))
print(len(my_string))
