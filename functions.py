from package.module import courses

x = print('Hello')
y = list()

print(type(x))
print(type(y))

print(x)
print(y)

my_list = [1, 2, 3]
my_list.append(4)
print(my_list)


def greeting():
    print('Привет тебе, добрый человек!')
    print('Как твои дела?:-)')


greeting()
help(greeting)


def greeting_with_name(name):
    print(f'Привет тебе, {name}!')
    print('Как твои дела?:-)')


greeting_with_name('Васек')
help(greeting_with_name)


def greeting_with_default_name(name='добрый человек'):
    print(f'Привет тебе, {name}!')
    print('Как твои дела?:-)')


greeting_with_default_name('Васек')
greeting_with_default_name()
x = greeting_with_default_name()
print(x)


def currency_convertor(value, currency='USD', value_in_grn=False):
    if value_in_grn:
        result = value/courses[currency]
    else:
        result = courses[currency] * value
    return round(result, 2)


result = currency_convertor(100, 'USD', True)
print(result)

result = currency_convertor(100, value_in_grn=False)
print(result)

result = currency_convertor(100, 'EUR')
print(result)

result = currency_convertor(100, currency='EUR')
print(result)
