from package import module

print(module)
print(module.courses)

from package.module import courses, cars

print(courses)
print(cars)

import random
from useful_operators import my_list

print(random.random())
print(random.randint(50, 100))
print(random.randrange(50, 100, 5))
print(random.choice(my_list))

import math
print(math.ceil(5.6))  # округление вверх
print(math.ceil(5.4))
print(math.floor(5.4))  # округление вниз
print(math.pi)
print(round(5.4))
print(round(5.6))

import datetime
print(datetime.datetime.today())
print(datetime.date.today())

from datetime import datetime, date
print(datetime.today())
print(date.today())
